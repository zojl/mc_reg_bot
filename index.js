const Discord        = require('discord.js');
const Sequelize      = require('sequelize');
const fs             = require('fs');
const uuidv4         = require('uuid/v4');
const crypto         = require('crypto');
const https          = require('https');

const envfile = __dirname + '/.env';
if (fs.existsSync(envfile)) {
	require('dotenv').config({path: envfile});
}

const client = new Discord.Client();

const db = new Sequelize(
    process.env.MYSQL_DB,
    process.env.MYSQL_USERNAME,
    process.env.MYSQL_PASSWORD,
    {
        dialect: 'mysql',
        host: process.env.MYSQL_HOST,
        port: process.env.MYSQL_PORT,
        pool: {
            max: 5,
            min: 0,
            acquire: 30000,
            idle: 10000
        },
        operatorsAliases: false,
        logging: false
    }
);

let model = {};
const modelsFolder = './app/models/';
fs.readdir(modelsFolder, (err, files) => {
    files.forEach(file => {
        filename = file.split('.').slice(0, -1).join('.')
        model[filename] = require(modelsFolder + filename)(db, Sequelize);
        model[filename].sync({alter: true});
    });
});

let commandHandlers = {};
let addCommandHandler = (command, functionBody) => {
  commandHandlers[command] = functionBody;
}


client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

let banUser = (discordID) => {
	model.users.findOne({
		where: {
			discordId: discordID
		}
	}).then(async(user) => {
		if (user){
			user.update({
				active: 0,
				accessToken: null
			});
			console.log(`banned user ${user.username}`);
		}
	});
}

let unbanUser = (discordID) => {
	let ignorelist = fs.readFileSync('/opt/files/ignorelist').toString().replace("\r", "").split("\n");
	if (ignorelist.includes(discordID))
		return false;
	model.users.findOne({
		where: {
			discordId: discordID
		}
	}).then(async(user) => {
		if (user){
			user.update({
				active: 1
			});
			console.log(`unbanned user ${user.username}`);
		}
	});
}

let isCuboeb = (userId) => {
	let guildUser = client.guilds.get(process.env.SERVER_ID).members.get(userId);
	if (typeof(guildUser) !== 'undefined') {
		return guildUser.roles.has(process.env.ROLE_ID);
	}
	return false;
}

addCommandHandler('регистрация', async (msg, words) => {
	if (!isCuboeb(msg.author.id)) {
		msg.reply('У тебя нет доступа к серверу. Попроси его у кого-нибудь из модераторов сервера дискорда.\nСм. https://discordapp.com/channels/420565457699864586/558440544104087584/558441574980255753');
		return;
	}

	if (words.length != 3) {
		msg.reply('Некорректно: напиши `Регистрация логин пароль`.');
		return;
	}

	re = /^[a-zA-Z0-9_]{4,16}$/g;
	if (!re.test(words[1])) {
		msg.reply('Логин должен быть от 4 до 16 символов.');
		return;
	}

	let usernameUsed = await model.users.count({where: {username: words[1]}});
	if (usernameUsed) {
		msg.reply('Этот логин уже занят!');
		return;
	}

	let discordUsed = await model.users.count({where: {discordID: msg.author.id}});
	if (discordUsed) {
		msg.reply('Ты уже зарегистрирован(а) под другим ником!');
		return;
	}
	
	let hash = crypto.createHash('sha1').update(words[2]).digest('hex');
	model.users.create({
		username: words[1],
		password: hash,
		discordID: msg.author.id,
		uuid: uuidv4()
	})
	console.log(`created user ${words[1]}`);

	msg.reply(`Успешная регистрация пользователя ${words[1]}`);
	client.channels.get(process.env.CHAT_ID).send(`${msg.author} зарегистрировался на сервере с ником ${words[1]}. Ура!`);
});

addCommandHandler('логин', async (msg, words) => {
	if (!isCuboeb(msg.author.id)) {
		msg.reply('У тебя нет доступа к серверу. Попроси его у кого-нибудь из модераторов сервера дискорда.\nСм. https://discordapp.com/channels/420565457699864586/558440544104087584/558441574980255753');
		return;
	}

	if (words.length != 2) {
		msg.reply('Некорректно: напиши `Логин новыйлогин`.');
		return;
	}

	re = /^[a-zA-Z0-9_]{4,16}$/g;
	if (!re.test(words[1])) {
		msg.reply('Логин должен быть от 4 до 16 символов.');
		return;
	}

	let usernameUsed = await model.users.count({where: {username: words[1]}});
	if (usernameUsed) {
		msg.reply('Этот логин уже занят!');
		return;
	}
	
	await model.users.findOne({
		where: {
			discordId: msg.author.id
		}
	}).then(async(user) => {
		if (user){
			user.update({
				username: words[1]
			});
			console.log(`updated username for user ${user.username}`);
			msg.reply(`Логин изменён. Для обновления логина надо перезапустить лаунчер.`);
		} else {
			msg.reply('Тебе бы сперва зарегистрироваться...');
			return;
		}
	});

	client.channels.get(process.env.CHAT_ID).send(`${msg.author} сменил ник на ${words[1]}.`);
});

addCommandHandler('пароль', async (msg, words) => {
	if (!isCuboeb(msg.author.id)) {
		msg.reply('У тебя нет доступа к серверу. Попроси его у кого-нибудь из модераторов сервера дискорда.');
		return;
	}

	if (words.length != 2) {
		msg.reply('Некорректно: напиши `пароль новыйпароль`.');
		return;
	}

	await model.users.findOne({
		where: {
			discordId: msg.author.id
		}
	}).then(async(user) => {
		if (user){
			let hash = await crypto.createHash('sha1').update(words[1]).digest('hex');
			user.update({
				password: hash
			});
			console.log(`updated password for user ${user.username}`);
			msg.reply(`Пароль изменён.`);
		} else {
			msg.reply('Тебе бы сперва зарегистрироваться...');
			return;
		}
	});
});

addCommandHandler('ктоя', async (msg, words) => {
	if (!isCuboeb(msg.author.id)) {
		msg.reply('У тебя нет доступа к серверу. Попроси его у кого-нибудь из модераторов сервера дискорда.');
		return;
	}

	await model.users.findOne({
		where: {
			discordId: msg.author.id
		}
	}).then(async(user) => {
		if (user){
			msg.reply(`Твой ник: ${user.username},\nТвой UUID: ${user.uuid}`);
		} else {
			msg.reply('Ты ещё не зарегистрирован(а).');
			return;
		}
	});
});

addCommandHandler('скин', async (msg, words) => {
	if (!isCuboeb(msg.author.id)) {
		msg.reply('У тебя нет доступа к серверу. Попроси его у кого-нибудь из модераторов сервера дискорда.');
		return;
	}

	await model.users.findOne({
		where: {
			discordId: msg.author.id
		}
	}).then(async(user) => {
		if (user){
			if (
				typeof(msg.attachments) !== 'undefined' &&
				msg.attachments.size == 1 &&
				typeof(msg.attachments.entries().next().value[1].width) != 'undefined' &&
				typeof(msg.attachments.entries().next().value[1].height) != 'undefined'
			){
				let attachment = msg.attachments.entries().next().value[1];
				let errors = [];
				let filename = attachment.filename;
				if (filename.split('.').slice(-1)[0].toLowerCase() !== 'png')
					errors.push('Расширение файла должно быть png');

				let skinSizes = ['64x32', '64x64', '128x64', '256x128', '512x256', '1024x512', '2048x1024', '4096x2048'];
				let currentSize = attachment.width + 'x' + attachment.height;
				if (!skinSizes.includes(currentSize))
					errors.push('Разрешение файла не соответствует возможным размерам скина');

				if (attachment.filesize > 3000000)
					errors.push('Слишком большой файл скина.');

				if (errors.length > 0)
					msg.reply('Так не пойдёт:\n' + errors.join('\n'));
				else {
					downloadSkin(attachment.url, '/opt/files/minecraft/skins/' + user.uuid + '.png');
					msg.reply('Скин обновлён. Перезайди на сервер.');
				}
			} else {
				msg.reply(`Чтобы сменить скин, надо скинуть файл скина с комментарием "скин". А не вот это вот.`);
			}
		} else {
			msg.reply('Ты ещё не зарегистрирован(а).');
			return;
		}
	});
});


function downloadSkin(url, dest) {
	var file = fs.createWriteStream(dest);
	var request = https.get(url, function(response) {
		response.pipe(file);
		file.on('finish', function() {
			file.close();
		});
	}).on('error', function(err) {
		fs.unlink(dest);
	});
};

client.on('guildMemberUpdate', (oldMember, newMember) => {
	let roleId = process.env.ROLE_ID;
	let oldRole = oldMember.roles.has(roleId);
	let newRole = newMember.roles.has(roleId);
	if (!newRole && oldRole) {
		banUser(oldMember.id);
		return;
	}
	if (newRole && !oldRole) {
		unbanUser(oldMember.id);
	}
});

client.on('guildBanAdd', (guild, user) => {
	if (guild.id == process.env.SERVER_ID) {
		banUser(user.id);
	}
});

client.on('guildMemberRemove', (member) => {
	if (member.guild.id == process.env.SERVER_ID) {
		banUser(member.id);
	}
});

client.on('message', (msg) => {
	if (msg.channel.type === 'dm' && !msg.author.bot){
		//read ignore list
		let ignorelist = fs.readFileSync('/opt/files/ignorelist').toString().replace("\r", "").split("\n");
		if (ignorelist.includes(msg.author.id))
			return msg.reply('Доступ запрещён.');
		let words = msg.content.split(' ');
		let command = words[0];
		command = command.toLowerCase().replace('ё', 'е');
		let allowedSymbols = /[a-zа-я]+/g;
		command = (command.match(allowedSymbols) || []).join('');
		if (typeof commandHandlers[command] !== 'undefined') {
			commandHandlers[command](msg, words);
		} else {
			msg.reply('Сорян, я не знаю такой команды');
		}
	}
	//console.log(msg);
});

client.login(process.env.TOKEN);
