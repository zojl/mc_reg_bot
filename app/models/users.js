module.exports = function(sequelize, DataTypes) {
  return sequelize.define('users', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    username: {
      type: DataTypes.STRING(128),
      allowNull: false,
      unique: true
    },
    uuid: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      unique: true
    },
    password: {
      type: DataTypes.CHAR(40),
      allowNull: false,
    },
    discordID: {
      type: DataTypes.BIGINT.UNSIGNED.ZEROFILL,
      allowNull: false,
      unique: true
    },
    accessToken: {
      type: DataTypes.CHAR(32),
      allowNull: true
    },
    serverID: {
      type: DataTypes.STRING(41),
      allowNull: true
    },
    active: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: true
    }
  }, {
    tableName: 'users'
  });
};
