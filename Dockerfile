FROM node

ENV MYSQL_HOST mysql
ENV MYSQL_PORT 3306
ENV MYSQL_USERNAME user
ENV MYSQL_PASSWORD password
ENV MYSQL_DB database
ENV TOKEN token
ENV SERVER_ID 12345
ENV ROLE_ID 12345
ENV CHAT_ID 12345

RUN mkdir /bot
COPY ./ /bot/
RUN cd /bot && npm install
CMD cd /bot && node index.js
